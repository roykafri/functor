class Functor(object):
    def __init__(self, func):
        self.func = func
        print("new Functor created")

    def __call__(self, x):
        return self.func(x)

    def __add__(self, other):
        if type(other) is Functor:
            return Functor(lambda x: self.func(x) + other.func(x))

    def __sub__(self, other):
        if type(other) is Functor:
            return Functor(lambda x: self.func(x) - other.func(x))

    def __mul__(self, other):
        if type(other) is Functor:
            return Functor(lambda x: self.func(other.func(x)))

def main():
    f1 = lambda x : x%3
    f2 = lambda x : x+3
    g1 = Functor(f1)
    g2 = Functor(f2)
    g3 = g1 + g2
    print(type(g3))
    print (g3(5) == f1(5) + f2(5))
    print(g3(5))
    g5 = g1 * g2
    print(g5(5))

if __name__ == '__main__':
    main()

